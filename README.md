# vp-test-tool

## Install

### from .zip

Extract the .zip file, open extensions manager of your broswer (both chrome and edge are ok), enable developer mode, then click `Load unpacked` and select `dist/` folder you extracted previously.

### from source code

Make sure you have installed Node.js and npm, then run:
```
npm i && npm run build
```
then you will get a `dist/` folder. Open extensions manager of your broswer (both chrome and edge are ok), enable developer mode, then click `Load unpacked` and select the `dist/`.

## Usage

### basic usage

Land the page which you will go through the test work, open devtools, select the tab named `VP test tool`, you will see the interface like:

![image.png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/2888738/attachments/image.png)

refresh your page, the interface will switch to:

![image (2).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/2888738/attachments/image%20%282%29.png)  

We know when we write a VP test spec we mainly care about two parts: interceptions and tests (exactly steps of test case).

For the interceptions part, the tool will automatically gather the response data from the page according by the response config (this config is allowed to be customized which will be illustrated later):

![image (3).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/2888738/attachments/image%20%283%29.png) 

For the tests part, or rather steps part, which mainly composed by `action`, `element`, `comment`, `validateBaseline`, the tool provides a form to gather them:

![image (4).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/2888738/attachments/image%20%284%29.png) 

the field `action`, `comment` and `validateBaseline` need to be filled manually, for the `element`, you can press and hold key `F2` then left click on the target element to automatically generate the DOM path for it.

Keep in mind: moving mouse to select certain element as the target element; using right click to select the parent element of the current target element as target element; using left click to generate the DOM path for the target element.

You will see the DOM path on the interface after you do a left clicking:

![image (5).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/2888738/attachments/image%20%286%29.png) 

The item has grey background will be composed to the DOM path that finally written in `element` field:

```
div[class*=EntryPoint-DS-] div#banner > div[class*=slide-info-wrap] > div[class*=description] > h1[class*=title]
```

Left click to select or unselect certain item.

When you finished the current step, click the button `Next step` to record the current step and create the next step. When you finished all the steps, fill in a filename then click `Generate` at the top of the interface:

![image (6).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/3767869/attachments/image%20%286%29.png) 

your VP test spec will be downloaded.

### customized

#### data config

Create a json file following this format:

```
{
    "localStorage": [
        "key1",
        "key2"
    ],
    "response": [
        "regex1",
        "regex2"
    ]
}
```

upload it through the data config uploader before you refresh your page you working on:

![image (7).png](https://dev.azure.com/msasg/89cacd35-bda7-4d42-be59-367d99273382/_apis/git/repositories/f8990680-0bb3-475b-a4b2-33bb08e0bf7b/pullRequests/3767869/attachments/image%20%287%29.png)

or named to `config.json` then paste it to the `dist/` folder and **reload the extension**.

#### spec template

Create a json file following VP test spec schema and named to `template.json` then paste it to the `dist/` folder and **reload the extension**.

### Utils

#### merge test spec

...

#### extract mock data

...

### Q&A

#### How about images intercepted?

The images intercepted will be transform to an SVG that has the same size with the original image, which being used as the response data wrote into the final test spec.

Example:
```
{
    "endpoint": {
        "regex": "https://assets.msn.com/staticsb/statics/latest/ugc/images/default-avatar.png"
    },
    "method": {
        "GET": {
            "type": "response",
            "response": {
                "contentType": "image/svg+xml",
                "status": 200,
                "data": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"240\" height=\"240\" fill=\"#000\"><rect xmlns=\"http://www.w3.org/2000/svg\" width=\"240\" height=\"240\"/></svg>"
            }
        }
    }
}
```

## Development

Make sure you have installed Node.js and npm, then run:
```
npm i && npm run watch
```
Open extensions manager of your broswer (both chrome and edge are ok), enable developer mode, then click `Load unpacked` and select `dist/` folder.

- When you modify the `.tsx` files, reopen the devtools or switch out and in to apply the update.

- When you modify the `.ts` files, click `Reload` button on this extension item on extensions manage page, then close devtools, refresh the page which involved to debugging, finally reopen devtools, the update will be applied.