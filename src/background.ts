import { GlobalCommand } from "./type";
import { sendMessageToContentScript } from "./utils";

var openCount = 0;
chrome.runtime.onConnect.addListener(function (port) {
    if (port.name == "vp-test-tool") {
        openCount++;

        port.onDisconnect.addListener(function(port) {
            openCount--;
            if (openCount == 0) {
                sendMessageToContentScript<GlobalCommand>("command", { type: "RemoveKeyboardListener" })
            }
        });
    }
});