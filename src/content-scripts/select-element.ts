import { sendMessage } from "../utils"

const selectedElement = (() => {
  let elemStore: HTMLElement[] = []
  return {
    set (elem: HTMLElement | null | undefined) {
      if (!elem) {
        return
      }
      elemStore.push(elem)
    },
    latest () {
      return elemStore[elemStore.length - 1] || { style: { outline: "" } }
    },
    emptyExceptLatest () {
      const holder = elemStore.slice(0, elemStore.length - 1)
      elemStore = elemStore.slice(elemStore.length - 1)
      return holder
    },
    empty () {
      const holder = elemStore
      elemStore = []
      return holder
    }
  }
})()

/*** handler ***/
const Mouseover = (e: MouseEvent | Event) => {
  const elem = e.target as HTMLElement
  selectedElement.set(elem)
  selectedElement.emptyExceptLatest().forEach(elem => elem.style.outline = "")
  selectedElement.latest().style.outline = '1px solid #000'
}
const Mouseout = (e: MouseEvent | Event) => {
  const elem = e.target as HTMLElement
  elem.style.outline = ""
  selectedElement.empty().forEach(elem => elem.style.outline = "")
}
const RightClick = (e: MouseEvent | Event) => {
  e.preventDefault();
  e.stopImmediatePropagation()
  e.stopPropagation()
  selectedElement.set(
    selectedElement.latest().parentElement
    || ((selectedElement.latest().parentNode as ShadowRoot)?.host as HTMLElement)
  )
  selectedElement.emptyExceptLatest().forEach(elem => elem.style.outline = "")
  selectedElement.latest().style.outline = '1px solid #000'
}
const LeftClick = (e: MouseEvent | Event) => {
  e.preventDefault()
  e.stopImmediatePropagation()
  e.stopPropagation()
  console.log(selectedElement.latest())
  const res = getDomPath(selectedElement.latest())
  sendMessage("elementPath", res)
  return false
}

const OnOf = (obj: Document | ShadowRoot | null | undefined) => {
    if (!obj) {
        return
    }
    obj.addEventListener("mouseover", Mouseover, true)
    obj.addEventListener("mouseout", Mouseout, true)
    if (obj === document) {
      obj.addEventListener("contextmenu", RightClick, true)
      obj.addEventListener("click", LeftClick, true)
    }
}
const OffOf = (obj: Document | ShadowRoot | null | undefined) => {
    if (!obj) {
        return
    }
    obj.removeEventListener("mouseover", Mouseover, true)
    obj.removeEventListener("mouseout", Mouseout, true)
    obj.removeEventListener("contextmenu", RightClick, true)
    obj.removeEventListener("click", LeftClick, true)
}

const On = () => {
    OnOf(document)
    getAllShadowRootOf(document.body).forEach(e => OnOf(e))
}
const Off = () => {
    OffOf(document)
    getAllShadowRootOf(document.body).forEach(e => OffOf(e))
    selectedElement.empty().forEach(elem => elem.style.outline = "")
}

export const ElementSelector = {
    on: On,
    off: Off
}

const getAllShadowRootOf = (elem: Element): ShadowRoot[] => {
  if (!elem) {
    return []
  }
  if (!elem.hasChildNodes && !elem.shadowRoot) {
    return []
  }
  return (elem.shadowRoot ? [elem.shadowRoot] : [])
    .concat(
      Array.from(elem.children)
        .concat(Array.from(elem.shadowRoot ? elem.shadowRoot.children : []))
        .flatMap(child => getAllShadowRootOf(child))
    )
}

/*** https://gist.github.com/karlgroves/7544592 ***/
export function getDomPath(el: any) {
  var stack = [];
  while ( el.parentNode != null || el.host != undefined ) {
    if (!el.parentNode) {
      el = el.host
      continue
    }
    var sibCount = 0;
    var sibIndex = 0;
    var index = 0
    for ( var i = 0; i < el.parentNode.children.length; i++ ) {
      var sib = el.parentNode.children[i];
      if ( sib.nodeName == el.nodeName ) {
        if ( sib === el ) {
          sibIndex = sibCount;
          index = i;
        }
        sibCount++;
      }
    }
    if (el.hasAttribute('part')) {
      stack.unshift({
        selector: el.nodeName.toLowerCase() + `[part=${el.getAttribute('part')}]`,
      })
    } else if ( el.hasAttribute('id') && el.id != '' ) {
      stack.unshift({
        selector: el.nodeName.toLowerCase() + '#' + el.id,
      })
    } else if (el.classList.length !== 0) {
      const className = Array.from(el.classList as DOMTokenList)
        .find((e: string) => e.indexOf("-DS-") !== -1)
        || el.classList[0]
      const partOfClassName = /(.*-DS-)/.exec(className)?.[0] || className
      stack.unshift({
        selector: el.nodeName.toLowerCase() + (partOfClassName ? `[class*=${partOfClassName}]` : ""),
        el,
        sibIndex,
        sibCount,
        index
      });
    } else if ( sibCount > 1 ) {
      stack.unshift({
        selector: el.nodeName.toLowerCase() + `:nth-child(${index + 1})`
      });
    } else {
      stack.unshift({
        selector: el.nodeName.toLowerCase()
      });
    }
    el = el.parentNode
  }
  return stack.reduce((path: string, item) => {
    let nextPath = path ? `${path} > ${item.selector}` : item.selector
    if (item.el && item.sibCount && item.sibCount > 1) {
      // const nodes = Array.from(document.querySelectorAll(nextPath))
      // if (nodes.length > 1) {
        // const n = nodes.findIndex(e => e === item.el)
        nextPath += `:nth-child(${item.index! + 1})`
      // }
    }
    return nextPath
  }, "")
  // return stack.slice(0);
}
