import { ElementSelector } from "./content-scripts/select-element"
import { GlobalCommand } from "./type"
import { receiveMessage, sendMessage } from "./utils"

const listener = receiveMessage<GlobalCommand>("command", (command, sender, sendRes) => {
  switch(command.type) {
    case "OpenElementSelector":
      ElementSelector.on()
      break
    case "CloseElementSelector":
      ElementSelector.off()
      break
    case "AddKeyboardListener":
      document.addEventListener("keydown", keydownHandler)
      document.addEventListener("keyup", keyupHandler)
      break
    case "RemoveKeyboardListener":
      document.removeEventListener("keydown", keydownHandler)
      document.removeEventListener("keyup", keyupHandler)
      ElementSelector.off()
      break
    case "SendEndpointField":
      sendMessage("pageLocation", JSON.stringify(window.location))
      break
  }
  sendRes("OK")
})
listener.on()

sendMessage("prob", "")

const keydownHandler = (e: KeyboardEvent) => {
  if (e.key === "F2") {
    ElementSelector.on()
  }
}
const keyupHandler = (e: KeyboardEvent) => {
  if (e.key === "F2") {
    ElementSelector.off()
  }
}