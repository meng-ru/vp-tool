import React, { useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { receiveMessage } from "../utils";
import { BasicFormTextAreaStyles, InputStyles } from "./styles";
import { BasicFormDataModel } from "./type";

type Props = {
  data: BasicFormDataModel,
  setData: React.Dispatch<React.SetStateAction<BasicFormDataModel>>
}

export const BasicForm = ({data, setData}: Props) => {
  const update = useCallback((key: keyof BasicFormDataModel, value: any) => {
    setData((oldValue) => ({
      ...oldValue,
      [key]: value
    }))
  }, [setData])

  return (
    <>
    <table>
      <tr>
        <td>appType: </td>
        <td>{data.appType}</td>
      </tr>
      <tr>
        <td>areaPath: </td>
        <td>{data.areaPath}</td>
      </tr>
      <tr>
        <td>suiteTitle: </td>
        <td>
          <input style={InputStyles} type="text" value={data.suiteTitle} onChange={(e) => {
            update("suiteTitle", e.currentTarget.value.slice(0, 45))
          }} />
        </td>
      </tr>
      <tr>
        <td>startEndpointByEnv.prod: </td>
        <td>
          <textarea style={BasicFormTextAreaStyles} value={data.prod} onChange={(e) => update("prod", e.currentTarget.value)}></textarea>
        </td>
      </tr>
      <tr>
        <td>startEndpointByEnv.local: </td>
        <td>
          <textarea style={BasicFormTextAreaStyles} value={data.local} onChange={(e) => update("local", e.currentTarget.value)}></textarea>
        </td>
      </tr>
    </table>
    </>
  );
};
