import React, { useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { MultiTextInput } from "./MultiTextInput";
import { ConfigFormDataModel } from "./type";

type Props = {
  data: ConfigFormDataModel,
  setData: React.Dispatch<React.SetStateAction<ConfigFormDataModel>>
}

export const ConfigForm = ({data, setData}: Props) => {
  const updateArray = useCallback((key: keyof ConfigFormDataModel, index: number, value?: any) => {
    if (!Array.isArray(data[key])) {
      return
    }
    const newArray = data[key].slice() as any[]
    if (value === undefined) {
      index = index === -1 ? data[key].length - 1 : index
      newArray.splice(index, 1)
    } else {
      index = index === -1 ? data[key].length : index
      newArray[index] = value
    }
    setData((prev) => ({
      ...prev,
      [key]: newArray
    }))
  }, [data, setData])

  return (
    <>
    <table>
      <tr>
        <td style={{ verticalAlign: "top"}}>localStorage: </td>
        <td>
            <MultiTextInput
                data={data.localStorage}
                setData={(index, value) => updateArray("localStorage", index, value)} />
        </td>
      </tr>
      <tr>
        <td style={{ verticalAlign: "top"}}>response: </td>
        <td>
          <div style={{ maxHeight: "100px", overflowY: "auto"}}>{data.response.map(item => (<><span>{item}</span><br /></>))}</div>
        </td>
      </tr>
    </table>
    </>
  );
};
