import React, { useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";

type Props = {
  data: string,
  setData: React.Dispatch<React.SetStateAction<string>>
}

const genStoreFromPath = (path: string) => {
    const store = path.split(" > ").map(selector => ({ selector, checked: false}))

    for (let i = store.length - 1; i >= store.length - Math.min(5, store.length); i--) {
        store[i].checked = true
        if (store[i].selector.indexOf("#") !== -1) {
            break
        }
    }

    return store
}

export const ElementPathEditor = ({ data, setData }: Props) => {
    const [store, setStore] = useState(genStoreFromPath(data))

    useEffect(() => {
        setStore(genStoreFromPath(data))
    }, [data])

    useEffect(() => {
        setData(store.reduce((res: { prevChecked: boolean, resPath: string }, next) => ({
            prevChecked: next.checked,
            resPath: next.checked
                ? `${res.resPath}${res.prevChecked ? " > " : " "}${next.selector}`
                : res.resPath
        }), { prevChecked: false, resPath: "" }).resPath.trim())
    }, [store])

    return <div>
        { store.map((item, index) => (
            <label className="selector-item" key={item.selector + index}>
                <input
                    type="checkbox"
                    value={item.selector}
                    checked={item.checked}
                    onChange={(e) => {
                        item.checked = e.currentTarget.checked
                        setStore(store.slice())
                    }} />
                <span className="selector">{item.selector}</span>
            </label>
        ))}
        <style>{`
            .selector-item {
                display: inline-block;
                position: relative;
                margin: 1px 3px;
            }
            .selector-item:hover {
                background-color: #efefef;
            }
            .selector-item > * {
                cursor: pointer;
            }
            .selector-item input {
                width: 100%;
                height: 100%;
                opacity: 0;
                position: absolute;
                top: 0;
                left: 0; 
            }
            .selector-item input:checked ~ .selector {
                background-color: #ddd;
            }
            .selector-item .selector {
                padding: 0px 3px;
            }
        `}</style>
    </div>
}