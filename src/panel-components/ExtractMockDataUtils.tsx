import React, { ChangeEvent, useCallback, useEffect, useRef, useState } from "react";
import JSZip from "jszip"
import { ResponseConfig, TestSpec } from "./type";
import { readFileAsText } from "./utils";
import { saveAs } from "file-saver";

type MiddleData = {
    endpoint: string
    method: Method
    data: any
    filename?: string
    contentType?: string
}

type Interception = TestSpec["interceptions"][number]

type Method = "GET" | "POST"

type NameTable = {[name: string]: number}

const isSameMiddleData = (comparator: MiddleData, allMiddleDatas: MiddleData[]) =>
    allMiddleDatas.some(item =>
        comparator.endpoint === item.endpoint
            && comparator.method === item.method
            && JSON.stringify(item.data) === JSON.stringify(comparator.data) 
    )

const genFilename = (urlPattern: string, method: string, nameTable: NameTable) => {
    let name = /(http|https):\/\/([^\/]*)\/(?<path>.*)/
        .exec(urlPattern)?.groups?.path
        ?.replace(/[^\w]+/g, "-")
    if (!name) {
        return ""
    }
    name = `${name}-${method}`
    if (!nameTable[name]) {
        nameTable[name] = 0
    }
    ++nameTable[name]
    return `${name}-${nameTable[name]}.json`
}

const reorganizeInterceptions = (
    parent: TestSpec | TestSpec["tests"][number],
    allMiddleDatas: MiddleData[],
    nameTable: NameTable
) => {
    if (!parent.interceptions) {
        return
    }
    const needToBeExtracted = parent.interceptions.filter(item =>
        item.method.POST?.type === "response"
        || item.method.GET?.type === "response"
    )
    const rested = parent.interceptions.filter(item =>
        ! (item.method.POST?.type === "response"
        || item.method.GET?.type === "response")
    )
    const newMiddleDatas = needToBeExtracted.flatMap(interception => genMiddleDatas(interception))
        .filter(item => !isSameMiddleData(item, allMiddleDatas))
        .map(item => {
            item.filename = genFilename(item.endpoint, item.method, nameTable)
            return item
        })
        .filter(item => item.filename)
    allMiddleDatas.push.apply(allMiddleDatas, newMiddleDatas)
    parent.interceptions = rested.concat(newMiddleDatas.map(item => genInterception(item)))
}

const genMiddleDatas = (originalInterception: Interception): MiddleData[] => {
    const endpoint = originalInterception.endpoint.regex
    return Object.keys(originalInterception.method).map(method => ({
        endpoint,
        method: method as Method,
        data: originalInterception.method[method as Method]?.response.data,
        contentType: originalInterception.method[method as Method]?.response.contentType
    }))
}

const genInterception = (middleData: MiddleData): Interception => {
    return {
        endpoint: {
            regex: middleData.endpoint
        },
        method: {
            [middleData.method]: {
                type: "response",
                response: {
                    dataFile: `data/${middleData.filename}`,
                    contentType: middleData.contentType,
                    headers: {
                        "Access-Control-Allow-Origin": "*"
                    },
                    status: 200
                }
            }
        }
    }
}

export const ExtractMockDataUtils = () => {
  const [errMsg, setErrMsg] = useState("")
  const [res, setRes] = useState("")
  const [filename, setFilename] = useState("")
  const zip = useRef(new JSZip())

  const handleConfigLoad = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
        readFileAsText(e.target.files[0])
            .then(res => {
                setRes(res)
                zip.current = new JSZip()
                const spec = JSON.parse(res) as TestSpec
                const allMiddleDatas: MiddleData[] = []
                const nameTable = {}
                reorganizeInterceptions(spec, allMiddleDatas, nameTable)
                spec.tests.forEach(testCase => reorganizeInterceptions(testCase, allMiddleDatas, nameTable))

                zip.current.file(e.target.files![0].name, JSON.stringify(spec))
                allMiddleDatas.forEach(item => zip.current.folder("data")?.file(item.filename as string, JSON.stringify(item.data)))
                setFilename(e.target.files![0].name)
            })
            .catch(e => {
                setErrMsg(e.message)
            })
    }
  }, [])
  return (
    <>
        <input type="file" onChange={handleConfigLoad} /><br />
        <button onClick={() => {
            if (!res) {
                setErrMsg(`Error: there are no files selected.`)
                return
            }
            zip.current.generateAsync({type: "blob"})
                .then(content => saveAs(content, `${filename}.zip`))
        }}>Download output</button>
        { errMsg && <div>{errMsg}</div> }
    </>
  );
}