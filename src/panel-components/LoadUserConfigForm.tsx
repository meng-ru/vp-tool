import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { ConfigFormDataModel } from "./type";

type Props = {
  data: ConfigFormDataModel,
  setData: React.Dispatch<React.SetStateAction<ConfigFormDataModel>>
}

export const LoadUserConfigForm = ({data, setData}: Props) => {
  const [errMsg, setErrMsg] = useState("")

  const handleConfigLoad = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const fileReader = new FileReader()
      fileReader.onload = () => {
        let userConfig
        try {
          userConfig = JSON.parse(fileReader.result as string) as ConfigFormDataModel
          if (!userConfig.localStorage || !userConfig.response) {
            throw new Error("config data is invalid.")
          }
          userConfig = {
            localStorage: userConfig.localStorage,
            response: userConfig.response
          }
          if (userConfig) {
            setData(userConfig)
            setErrMsg("√ load successfully")
          }
        } catch (e: any) {
          setErrMsg(`Error: ${e.message}`)
        }
      }
      fileReader.readAsText(e.target.files[0])
    }
  }, [])

  return (
    <>
        <h3>data config: <input type="file" onChange={handleConfigLoad} /></h3>
        { errMsg && <div>{errMsg}</div> }
    </>
  );
};