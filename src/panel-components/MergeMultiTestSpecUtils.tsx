import { saveAs } from "file-saver";
import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { TestSpec } from "./type";
import { readFileAsText } from "./utils";

export const MergeMultiTestSpecUtils = () => {
  const [errMsg, setErrMsg] = useState("")
  const [res, setRes] = useState<TestSpec>()
  const [filename, setFilename] = useState("")

  const handleConfigLoad = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
       Promise.all(Array.from(e.target.files).map(blob => readFileAsText(blob)))
        .then(res => {
            const specs = res.map(str => JSON.parse(str)) as TestSpec[]
            for (let i = 0; i < specs.length - 1; i++) {
                if (specs[i].startEndpointByEnv.prod !== specs[i + 1].startEndpointByEnv.prod
                    || specs[i].startEndpointByEnv.local !== specs[i + 1].startEndpointByEnv.local) {
                    throw new Error("test specs who will be merged should have same value in prod and local fields")
                }
            }
            specs.forEach(spec => {
                const localInterceptions = spec.interceptions.filter(item =>
                    item.method.POST?.type === "response"
                    || item.method.GET?.type === "response"
                )
                const globalInterceptions = spec.interceptions.filter(item =>
                    ! (item.method.POST?.type === "response"
                    || item.method.GET?.type === "response")
                )
                spec.interceptions = globalInterceptions
                spec.tests[0].interceptions = localInterceptions
            })
            const newSpec = specs.pop() as TestSpec
            specs.reduce((res, spec) => {
                const shouldBeMerged = spec.interceptions.filter(specItem =>
                    res.interceptions.find(resItem =>
                        resItem.endpoint.regex === specItem.endpoint.regex
                    ) === undefined
                )
                res.interceptions = res.interceptions.concat(shouldBeMerged)
                res.tests.push(spec.tests[0])
                return res
            }, newSpec)
            setRes(newSpec)
            setErrMsg("√ load successfully")
        })
        .catch(e => {
          setErrMsg(`Error: ${e.message}, please check the files you selected.`)
        })
    }
  }, [])

  return (
    <>
        <input type="file" onChange={handleConfigLoad} multiple /><br />
        filename: <input value={filename} onChange={(e) => setFilename(e.target.value)} />.spec.json
        <button onClick={() => {
            if (!res) {
                setErrMsg(`Error: there are no files selected.`)
                return
            }
            saveAs(
                new Blob([JSON.stringify(res)], { type: "text/plain;charset=utf-8" }),
                `${filename}.spec.json`
            )
        }}>Download output</button>
        { errMsg && <div>{errMsg}</div> }
    </>
  );
};
