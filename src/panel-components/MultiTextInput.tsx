import React from "react";
import ReactDOM from "react-dom";
import { InputStyles } from "./styles";

type Props = {
    data: string[],
    setData: (index: number, value?: string) => void
}

export const MultiTextInput = ({data, setData}: Props) => {
    return<>
        {
            data.map((item, index) => (
                <>
                <input type="text" key={index}
                style={InputStyles}
                value={data[index]}
                onChange={(e) => setData(index, e.currentTarget.value)}
                /><br/>
                </>
            ))
        }
        <button onClick={() => setData(-1, "")}>+</button>
        {
            data.length > 1
            && <button onClick={() => setData(-1)}>-</button>
        }
    </>
}
