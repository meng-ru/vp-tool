import React, { useCallback, useEffect } from "react";
import ReactDOM from "react-dom";
import { StepActionFormDataModel } from "./type";

type Props = {
  data: StepActionFormDataModel,
  setData: React.Dispatch<React.SetStateAction<StepActionFormDataModel>>
}

export const StepActionForm = ({data, setData}: Props) => {
    const actions: { name: StepActionFormDataModel["type"], paramName?: "value" | "files" }[] = [
        { name: "assertPresence" },
        { name: "leftClick" },
        { name: "hover" },
        { name: "verticalScroll", paramName: "value" },
        { name: "assertAbsence" },
        { name: "pressKey", paramName: "value" },
        { name: "typeKeys", paramName: "value" },
        { name: "uploadFiles", paramName: "files" }
    ]

    const update = useCallback((key: keyof StepActionFormDataModel, value: any) => {
        setData((oldValue) => ({
        ...oldValue,
        [key]: value
        }))
    }, [setData])

    // todo
    const verticalScrollHandler = (e: MouseEvent) => {
    }

    useEffect(() => {
        if (data.type === "verticalScroll") {
        }
    }, [data.type])

  return (
    <div>
        {
            actions.map(item => (<>
                <input type="radio" name="action" value={item.name}
                    checked={item.name === data.type}
                    onChange={(e) => {
                        update("type", e.currentTarget.value)
                        update("value", "")
                    }}/>
                <label>{item.name}</label>
                {
                    item.paramName && (item.name === data.type) &&
                    <input value={data[item.paramName]}
                        onChange={(e) => update(item.paramName as keyof StepActionFormDataModel, e.target.value)} />
                }<br />
            </>))
        }
    </div>
  );
};

