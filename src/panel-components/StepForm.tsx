import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { StepActionForm } from "./StepActionForm";
import { StepFormDataModel } from "./type";
import { useStepFormData } from "./useData";
import { useReceiveElementPath } from "./useReceiveElementPath";
import { ElementPathEditor } from "./ElementPathEditor";

type Props = {
  data: StepFormDataModel
  setData: ReturnType<typeof useStepFormData>
}

export const StepForm = ({data, setData}: Props) => {
    const [newPath, setNewPath] = useState("")

    useReceiveElementPath(setNewPath)

    useEffect(() => {
        if (!data.element) {
            setNewPath("")
        }
    }, [data.element])

    return (
        <div>
            <div>
                Element: <br />
                { newPath
                    ? <ElementPathEditor data={newPath} setData={setData.setElement} />
                    : "Use F2 to get the DOM path, which will be shown here."
                }
            </div>
            <StepActionForm data={data.action} setData={setData.setStepActionFormData} />
            <div>
                Validate baseline: 
                <input type="radio" name="baseline" value="true"
                    checked={data.validateBaseline === true}
                    onChange={(e) => setData.setValidateBaseline(true)} />
                <label>Yes</label>
                <input type="radio" name="baseline" value="false"
                    checked={data.validateBaseline === false}
                    onChange={() => setData.setValidateBaseline(false)} />
                <label>No</label>
            </div>
            <div>
                comment: <br />
                <textarea style={{ width: "100%", height: "30px", fontSize: "1.1em"}}
                    value={data.comment} onChange={(e) => setData.setComment(e.currentTarget.value)}></textarea>
            </div>
        </div>
    );
};


