import React, { useState } from "react";
import ReactDOM from "react-dom";
import { StepFormDataModel } from "./type";

type Props = {
  data: StepFormDataModel[]
  setData: React.Dispatch<React.SetStateAction<StepFormDataModel[]>>
}

export const StepRecordTable = ({data, setData}: Props) => {

  const tdStyle = {
      border: "1px solid #ddd",
      padding: "1px 3px"
  }

  return <>
    <table style={{borderCollapse: "collapse"}}>
      <tr>
        <th>step</th>
        <th style={{width: "200px"}}>element</th>
        <th>action</th>
        <th>action.value</th>
        <th>validateBaseline</th>
        <th style={{width: "200px"}}>comment</th>
      </tr>
      {
          data.map((item, index) => (
              <tr>
                  <td align="center" style={tdStyle}>{index}</td>
                  <td align="center" style={tdStyle}>{item.element}</td>
                  <td align="center" style={tdStyle}>{item.action.type}</td>
                  <td align="center" style={tdStyle}>{item.action.value || item.action.files}</td>
                  <td align="center" style={tdStyle}>{String(item.validateBaseline)}</td>
                  <td align="center" style={tdStyle}>{item.comment}</td>
              </tr>
          ))
      }
    </table>
    <div>
      <button onClick={() => setData((old) => old.slice(0, old.length - 1))}>clear the latest</button>
      <button onClick={() => setData([])}>clear all steps</button>
    </div>
  </>;
};



