import React, { useCallback, useState } from "react";
import ReactDOM from "react-dom";
import { InputStyles } from "./styles";
import { TestCaseDataModel } from "./type";

type Props = {
  data: TestCaseDataModel,
  setData: React.Dispatch<React.SetStateAction<TestCaseDataModel>>
}

export const TestCaseForm = ({ data, setData}: Props) => {
    const update = useCallback((key: keyof TestCaseDataModel, value: any) => {
        setData((oldValue) => ({
        ...oldValue,
        [key]: value
        }))
    }, [setData])

    return <>
        <table>
        <tr>
            <td>test case name: </td>
            <td>
            <input style={InputStyles} type="text" value={data.testCase} onChange={(e) => {
                update("testCase", e.currentTarget.value.slice(0, 185))
            }} />
            </td>
        </tr>
        <tr>
            <td>start condition: </td>
            <td>
            <input style={InputStyles} type="text" value={data.startCondition}
                onChange={(e) => update("startCondition", e.currentTarget.value)} />
            </td>
        </tr>
        </table>
    </>
}