import { ConfigFormDataModel, StepActionFormDataModel, TestSpec } from "./type";
import { BufItem } from "./useRequestStream";

export default {
    ConfigForm: {
        localStorage (input: ConfigFormDataModel["localStorage"]) {
            return input.filter(key => key !== "")
        }
    },

    BufItems (input: BufItem[]): TestSpec["interceptions"] {
        console.log(input)
        return input.map(item => ({
            endpoint: {
                regex: item.regex
            },
            method: {
                [item.method]: {
                    type: "response",
                    response: {
                        data: JSON.parse(item.data),
                        status: 200,
                        contentType: item.contentType,
                        headers: {
                            "Access-Control-Allow-Origin": "*"
                        }
                    }
                }
            }
        }))
    },

    StepForm: {
        action (input: StepActionFormDataModel): TestSpec["tests"][number]["steps"][number]["action"] {
            const res: TestSpec["tests"][number]["steps"][number]["action"] = {
                type: input.type
            }
            if (input.value) {
                res.value = input.value
            }
            if (input.files) {
                res.files = input.files.split(",").map(item => item.trim())
            }
            return res
        }
    }
}

const responseBufItemToConfig = () => {}