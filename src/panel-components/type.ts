export type BasicFormDataModel = {
    appType: string
    areaPath: string
    suiteTitle: string
    prod: string
    local: string
}

export type ConfigFormDataModel = {
    localStorage: string[]
    response: string[]
}

export type TestCaseDataModel = {
    testCase: string
    startCondition: "networkidle" | "load"
}

export type StepActionFormDataModel = {
    type: 'leftClick' | 'assertAbsence' | 'assertPresence' | 'hover' | 'verticalScroll' | 'pressKey' | 'typeKeys' | 'uploadFiles'
    value?: string
    files?: string
}

export type StepFormDataModel = {
    action: StepActionFormDataModel
    validateBaseline: boolean
    comment: string
    element: string
}

type RequestOpreation = "abort" | "continue" | "response"
export type ResponseConfig = {
    dataFile?: string
    contentType?: string
    headers?: {
        "Access-Control-Allow-Origin"?: string
    },
    status: number
    data?: any
}
export type TestSpec = {
    $schema: string
    ownership: string
    appType: string
    areaPath: string
    enforceMock: boolean
    suiteTitle: string
    timeout: number
    startEndpointByEnv: {
        prod: string
        local: string
    }
    context: {
        _device: string[]
        market: string[]
        localStorage?: [{[key: string]: string}]
        _viewport?: string[]
        flightId: string[]
    }
    interceptions: {
        endpoint: {
            regex: string
        }
        method: {
            GET?: {
                type: RequestOpreation
                response: ResponseConfig
            }
            POST?: {
                type: RequestOpreation
                response: ResponseConfig
            }
            OPTIONS?: {
                type: RequestOpreation
                response: ResponseConfig
            }
        }
    }[]
    tests: {
        testCase: string
        startCondition?: string
        interceptions?: TestSpec["interceptions"]
        steps: {
            comment?: string
            action: {
                type: string
                value?: string | number
                files?: string[]
            }
            element: string
            validateBaseline?: boolean
        }[]
    }[]
}