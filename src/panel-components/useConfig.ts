import React, { useEffect, useMemo, useRef, useState } from "react";
import { ConfigFormDataModel } from "./type";

export const useConfig = (
    setConfigFormData: React.Dispatch<React.SetStateAction<ConfigFormDataModel>>
) => {
    useEffect(() => {
        fetch("./config.json")
            .then(res => res.json())
            .then((res: ConfigFormDataModel) => {
                setConfigFormData({
                    localStorage: res.localStorage,
                    response: res.response
                })
            })
            .catch(() => {})
    }, [])
}