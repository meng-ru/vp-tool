import React, { useEffect, useMemo, useState } from "react";
import { BasicFormDataModel, ConfigFormDataModel, StepActionFormDataModel, StepFormDataModel, TestCaseDataModel } from "./type";

export const useBasicFormData = () => {
  const [data, setData] = useState<BasicFormDataModel>({
    appType: "",
    areaPath: "",
    suiteTitle: "",
    prod: "",
    local: ""
  })
  return {
      basicFormData: data,
      setBasicFormData: setData
  }
}

export const useConfigFormData = () => {
  const [data, setData] = useState<ConfigFormDataModel>({
    localStorage: [],
    response: []
  })
  return {
      configFormData: data,
      setConfigFormData: setData
  }
}

export const useTestCaseFormData = () => {
  const [data, setData] = useState<TestCaseDataModel>({
    testCase: "",
    startCondition: "networkidle"
  })
  return {
    testCaseFormData: data,
    setTestCaseFormData: setData
  }
}

const useStepActionFormData = () => {
  const [data, setData] = useState<StepActionFormDataModel>({
    type: "leftClick"
  })
  return {
      stepActionFormData: data,
      setStepActionFormData: setData,
      readAndReset (): StepActionFormDataModel {
        const copy = {
          ...data
        }
        setData({
          type: "leftClick"
        })
        return copy
      }
  }
}
export const useStepFormData = () => {
  const {stepActionFormData,
    setStepActionFormData,
    readAndReset: readAndResetStepActionFormData} = useStepActionFormData()
  const [validateBaseline, setValidateBaseline] = useState(false)
  const [comment, setComment] = useState("")
  const [element, setElement] = useState("")

  const data = useMemo<StepFormDataModel>(() => ({
    action: stepActionFormData,
    validateBaseline,
    comment,
    element
  }), [stepActionFormData, validateBaseline, comment, element])

  return {
    stepFormData: data,
    stepActionFormData,
    setStepActionFormData,
    setValidateBaseline,
    setComment,
    setElement,
    readAndReset () {
      const copy = {
        ...data,
        action: readAndResetStepActionFormData()
      }
      setValidateBaseline(false)
      setComment("")
      setElement("")
      return copy
    }
  }
}