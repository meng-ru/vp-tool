import React, { useEffect, useMemo, useRef, useState } from "react";
import { BasicFormDataModel, ConfigFormDataModel, StepFormDataModel, TestCaseDataModel, TestSpec } from "./type";
import DataTransformer from "./data-transformer"
import { useRequestStream } from "./useRequestStream";
import { readLocalStorage } from "./utils";

const placeholder = {
    $schema: "",
    ownership: "",
    appType: "",
    areaPath: "",
    enforceMock: true,
    suiteTitle: "",
    timeout: 50000,
    startEndpointByEnv: {
        prod: "",
        local: ""
    },
    context: {
        _device: [
            "desktopChrome"
        ],
        market: [
            "en-us"
        ],
        flightId: []
    },
    interceptions: [],
    tests: []
} as TestSpec

export const useGenTestSpecJSON = (
    basicFormData: BasicFormDataModel,
    configFormData: ConfigFormDataModel,
    testCaseFormData: TestCaseDataModel,
    stepsData: StepFormDataModel[],
    setBasicFormData: React.Dispatch<React.SetStateAction<BasicFormDataModel>>
) => {
    const { localStorageData } = useLocalStorageGetter(configFormData.localStorage)
    const { readBuf: readResponseDataBuf } = useRequestStream(configFormData.response)

    const userTemplate = useRef<TestSpec>()

    useEffect(() => {
        fetch("./template.json")
            .then(res => res.json())
            .then(res => userTemplate.current = res)
            .catch(() => userTemplate.current = placeholder)
            .then(() => {
                setBasicFormData({
                    appType: userTemplate.current?.appType as string,
                    areaPath: userTemplate.current?.areaPath as string,
                    prod: userTemplate.current?.startEndpointByEnv.prod as string,
                    local: userTemplate.current?.startEndpointByEnv.local as string,
                    suiteTitle: ""
                })
            })
    }, [])

    const res = useMemo(() => {
        const template = userTemplate.current
        if (!template) {
            return placeholder
        }
        return {
            ...template,
            appType: basicFormData.appType || template.appType,
            areaPath: basicFormData.areaPath || template.areaPath,
            suiteTitle: basicFormData.suiteTitle,
            startEndpointByEnv: {
                ...template.startEndpointByEnv,
                prod: basicFormData.prod || template.startEndpointByEnv.prod,
                local: basicFormData.local || template.startEndpointByEnv.local
            },
            context: {
                ...template.context,
                localStorage: localStorageData
            },
            tests: [{
                testCase: testCaseFormData.testCase,
                startCondition: testCaseFormData.startCondition || undefined,
                steps: stepsData.map(item => ({ ...item, action: DataTransformer.StepForm.action(item.action)}))
            }]
        }
    }, [basicFormData, configFormData, testCaseFormData, stepsData, localStorageData, userTemplate.current])

    return {
        getResult (): TestSpec {
            return {
                ...res,
                interceptions: res.interceptions.concat(
                    DataTransformer.BufItems(readResponseDataBuf())
                )
            }
        }
    }
}

const useLocalStorageGetter = (keys: ConfigFormDataModel["localStorage"]) => {
    const [res, setRes] = useState<TestSpec["context"]["localStorage"]>([{}])

    useEffect(() => {
        readLocalStorage(DataTransformer.ConfigForm.localStorage(keys), (res) => {
            setRes([
                res.filter(item => item.value !== null)
                    .reduce((kv, item) => {
                        kv[item.key] = item.value
                        return kv
                    }, {})
            ])
        })
    }, [keys])

    return {
        localStorageData: res
    }
}
