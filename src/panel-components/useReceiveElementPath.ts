import React, { useEffect, useRef } from "react";
import { receiveMessage } from "../utils";

export const useReceiveElementPath = (setter: React.Dispatch<React.SetStateAction<string>>) => {
  useEffect(() => {
    const listener = receiveMessage<string>("elementPath", (elementPath) => {
      setter(elementPath)
    })
    listener.on()
    return () => listener.off()
  }, [])
}
