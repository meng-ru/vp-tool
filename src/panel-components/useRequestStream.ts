import React, { useEffect, useRef } from "react";
import { TestSpec } from "./type";

type Method = keyof TestSpec["interceptions"][number]["method"]

const getPatternWhoIsMatched = (urlPatterns: string[], url: string | null | undefined) => {
    if (!url) {
        return
    }
    return urlPatterns.find(pattern => new RegExp(pattern).test(url))
}

const isMethodExpected = (method: string) => {
    method = method.toUpperCase()
    return method === "GET" || method === "POST"
}

const removeSamePatternItem = (buffer: BufItem[], pattern: string, method: Method) => {
    const i = buffer.findIndex(item => item.regex === pattern && item.method === method);
    if (i !== -1) {
        buffer.splice(i, 1)
    }
}

export type BufItem = {
    method: Method
    contentType: string
    data: string
    regex: string
}

export const useRequestStream = (urlPatterns: string[]) => {
    const gather = useRef<BufItem[]>([])

    useEffect(() => {
        const consumeRequest = (request: chrome.devtools.network.Request) => {
            const matchedPattern = getPatternWhoIsMatched(urlPatterns, request.request.url)
            if (!matchedPattern) {
                return
            }
            if (!isMethodExpected(request.request.method)) {
                return
            }
            removeSamePatternItem(
                gather.current,
                matchedPattern,
                request.request.method?.toLocaleUpperCase() as Method
            )
            switch(request.response.content.mimeType) {
                case "application/json":
                case "text/plain":
                    request.getContent((content: string) => {
                        gather.current.push({
                            method: request.request.method?.toUpperCase() as BufItem["method"],
                            contentType: request.response.content.mimeType,
                            data: content,
                            regex: matchedPattern
                        })
                    })
                    break
                case "image/png":
                case "image/jpg":
                case "image/svg+xml":
                    // convert to svg with same size
                    const image = new Image()
                    image.onload = () => {
                        gather.current.push({
                            method: request.request.method?.toUpperCase() as BufItem["method"],
                            contentType: request.response.content.mimeType,
                            data: `<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"${image.width}\" height=\"${image.height}\" fill=\"#000\"><rect xmlns=\"http://www.w3.org/2000/svg\" width=\"${image.width}\" height=\"${image.height}\"/></svg>`,
                            regex: matchedPattern
                        })
                    }
                    image.src = request.request.url
                    break
            }
        }
        chrome.devtools.network.onRequestFinished.addListener(consumeRequest)
        return () => {
            chrome.devtools.network.onRequestFinished.removeListener(consumeRequest)
        }
    }, [urlPatterns])
    return {
        readBuf () {
            // const curBuf = buffer.current
            // buffer.current = []
            // return curBuf
            return gather.current
        }
    }
}