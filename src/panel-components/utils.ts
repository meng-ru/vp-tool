export const readLocalStorage = async (keys: string[], callback: (res: {[key: string]: string}[]) => void) => {
    const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
    const tab = tabs[0]; 

    chrome.scripting.executeScript({
        target: {
            tabId: tab.id as number
        },
        func: (keys) => {
            return keys.map(key => ({ key, value: localStorage.getItem(key) }))
        },
        args: [keys]
    }, (res) => {
        callback(res[0].result)
    });
}

export const readFileAsText = (blob: Blob) => {
    return new Promise((resolve: (res: string) => void, reject) => {
        const fileReader = new FileReader()
        fileReader.onload = () => {
            resolve(fileReader.result as string)
        }
        fileReader.readAsText(blob)
    })
}
