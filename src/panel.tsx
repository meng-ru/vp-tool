import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { BasicForm } from "./panel-components/BasicForm";
import { ConfigForm } from "./panel-components/ConfigForm";
import { StepForm } from "./panel-components/StepForm";
import { StepRecordTable } from "./panel-components/StepRecordTable";
import { TestCaseForm } from "./panel-components/TestCaseForm";
import { StepFormDataModel } from "./panel-components/type";
import { useBasicFormData, useConfigFormData, useStepFormData, useTestCaseFormData } from "./panel-components/useData";
import { useGenTestSpecJSON } from "./panel-components/useGenTestSpecJSON";
import { GlobalCommand } from "./type";
import { receiveMessage, sendMessageToContentScript } from "./utils";
import { saveAs } from 'file-saver'
import { LoadUserConfigForm } from "./panel-components/LoadUserConfigForm";
import { MergeMultiTestSpecUtils } from "./panel-components/MergeMultiTestSpecUtils";
import { useReceiveElementPath } from "./panel-components/useReceiveElementPath";
import { ExtractMockDataUtils } from "./panel-components/ExtractMockDataUtils";
import { useConfig } from "./panel-components/useConfig";

const Panel = () => {
  const { basicFormData, setBasicFormData } = useBasicFormData()
  const { configFormData, setConfigFormData } = useConfigFormData()
  const { testCaseFormData, setTestCaseFormData } = useTestCaseFormData()
  const stepFormDataAndSetter = useStepFormData()

  const [stepsData, setStepsData] = useState<StepFormDataModel[]>([])
  const [filename, setFilename] = useState("")
  const [isPageRefreshed, setIsPageRefreshed] = useState(false)
  const [elementPath, setElementPath] = useState("")

  useConfig(setConfigFormData)
  useReceiveElementPath(setElementPath)

  const { getResult } = useGenTestSpecJSON(
    basicFormData, configFormData, testCaseFormData, stepsData, setBasicFormData
  )

  useEffect(() => {
    sendMessageToContentScript<GlobalCommand>("command", { type: "AddKeyboardListener" })

    const listener = receiveMessage("prob", () => {
      sendMessageToContentScript<GlobalCommand>("command", { type: "AddKeyboardListener" })
      sendMessageToContentScript<GlobalCommand>("command", { type: "SendEndpointField" })
      setIsPageRefreshed(true)
    })
    listener.on()
    return () => listener.off()
  }, [])

  const readStepFormData = () => {
    stepsData.push(stepFormDataAndSetter.readAndReset())
    setStepsData(stepsData.slice())
  }

  return <>{ isPageRefreshed
    ? <>
      <div>
        filename: <input value={filename} onChange={(e) => setFilename(e.target.value)} />.spec.json
        <button onClick={() =>
          saveAs(
            new Blob([JSON.stringify(getResult())], { type: "text/plain;charset=utf-8" }),
            `${filename}.spec.json`
          )
        }>Generate</button>
      </div>

      <hr />

      <BasicForm data={basicFormData} setData={setBasicFormData} />

      <hr />

      <ConfigForm data={configFormData} setData={setConfigFormData} />

      <hr />

      <TestCaseForm data={testCaseFormData} setData={setTestCaseFormData} />

      <hr />

      <div style={{ position: "relative" }}>
        <h3>current step: {stepsData.length}</h3>
        <StepForm data={stepFormDataAndSetter.stepFormData} setData={stepFormDataAndSetter} />
        <button onClick={readStepFormData}>Next step</button>
        <p>steps JSON for copy:</p>
        <textarea
          style={{ width: "100%", fontSize: "12px" }}
          value={JSON.stringify(stepsData)}
          onMouseOver={(e) => e.currentTarget.select()} />
        <StepRecordTable data={stepsData} setData={setStepsData} />
      </div>
    </>
    : <>
        <div>Please refresh the page to enable VP test tool</div>
        <LoadUserConfigForm data={configFormData} setData={setConfigFormData} />
        <hr />
        <h2>Utils</h2>
        <h3>Merge test specs</h3>
        <MergeMultiTestSpecUtils />
        <h3>Element selector</h3>
        <p>{elementPath || "Use F2 to get the DOM path, which will be shown here."}</p>
        <h3>Extract mock datas</h3>
        <ExtractMockDataUtils />
      </>
  }</>
};

ReactDOM.render(
  <React.StrictMode>
    <Panel />
  </React.StrictMode>,
  document.getElementById("root")
);

