export type GlobalCommandType =
    "OpenElementSelector"
    | "CloseElementSelector"
    | "AddKeyboardListener"
    | "RemoveKeyboardListener"
    | "SendEndpointField"

export type GlobalCommand = {
    type: GlobalCommandType,
    payload?: string
}