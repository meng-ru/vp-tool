export const sendMessage = <T>(id: string, data: T, callback?: (res: any) => void) => {
    chrome.runtime.sendMessage({id, data}, callback)
}

export const sendMessageToContentScript = async <T>(id: string, data: T, callback?: (res: any) => void) => {
    const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
    const tab = tabs[0]; 
    if (tab.id === undefined) {
        return
    }
    chrome.tabs.sendMessage(tab.id, {id, data}, callback)
}

export const receiveMessage = <T>(
    id: string,
    handler: (
        msg: T,
        sender: chrome.runtime.MessageSender,
        sendRes: (res: any) => void
    ) => void,
) => {
    const consumer = (
        message: any,
        sender: chrome.runtime.MessageSender,
        sendResponse: (res: any) => void
    ) => {
        if (message.id === id) {
            handler(message.data, sender, sendResponse)
        }
    }
    return {
        on () {
            chrome.runtime.onMessage.addListener(consumer)
        },
        off () {
            chrome.runtime.onMessage.removeListener(consumer)
        }
    }
}