const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
var WebpackObfuscator = require('webpack-obfuscator');

module.exports = merge(common, {
    mode: 'production',
    plugins: [
        new WebpackObfuscator ({
            rotateStringArray: true
        }, ['panel.js', 'content_script.js', 'background.js', 'devtools.js', 'vendor.js'])
    ]
});